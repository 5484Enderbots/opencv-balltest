#!/bin/sh
set -eux
mkdir -p bin
g++ -o bin/ballfinder ballfinder.cpp -lopencv_videoio -lopencv_core -lopencv_imgproc -lopencv_objdetect -lopencv_highgui -Wall -Wextra
#g++ -o balltest balltest.cpp -lopencv_videoio -lopencv_core -lopencv_imgproc -lopencv_objdetect -lopencv_highgui -Wall -Wextra
#g++ -o blueballtest blueballtest.cpp -lopencv_videoio -lopencv_core -lopencv_imgproc -lopencv_objdetect -lopencv_highgui -Wall -Wextra
