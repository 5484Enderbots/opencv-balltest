#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
	// change this to capture(0) if you want to use a webcam
	VideoCapture capture(0);
	if (!capture.isOpened()) {
		cerr << "Error: can't open stream!" << endl;
		return 1;
	}
	
	int minH = 110;
	int minS = 100;
	int minV = 100;
	int maxH = 130;
	int maxS = 255;
	int maxV = 255;

	//namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	namedWindow("mask", CV_WINDOW_AUTOSIZE); 
	namedWindow("original", CV_WINDOW_AUTOSIZE); 
	/*
	cvCreateTrackbar("minH", "Control", &minH, 179); //Hue (0 - 179)
	cvCreateTrackbar("maxH", "Control", &maxH, 179);

	cvCreateTrackbar("minS", "Control", &minS, 255); //Saturation (0 - 255)
	cvCreateTrackbar("maxS", "Control", &maxS, 255);

	cvCreateTrackbar("minV", "Control", &minV, 255); //Value (0 - 255)
	cvCreateTrackbar("maxV", "Control", &maxV, 255);
	*/
	while (true) {
		Mat frame;

		if (!capture.read(frame)) {
			return 1;
		}

		Mat hsv;
		Mat mask;
		Mat grey;
		Mat canny_out;
		cvtColor(frame, hsv, COLOR_BGR2HSV);
		cvtColor(frame, grey, COLOR_BGR2GRAY);
		std::vector<KeyPoint> keypoints;


		inRange(hsv, Scalar(minH, minS, minV), Scalar(maxH, maxS, maxV), mask);
		
		//GaussianBlur( mask, mask, cv::Size(3, 3), 2, 2 );
		medianBlur(mask, mask, 9);

		//Canny(mask, canny_out, 100, 200, 3);
		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;
		findContours(mask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
			
		Point2f center;
		float radius;

		double maxArea = 0;
		double perimeter = -1;
		int index = 0;

		for ( int i = 0; i< contours.size(); i++ ) {
			double area = contourArea(contours[i], false);
			//perimeter = arcLength(contours[i], true);
			if (area > maxArea){
				maxArea = area;
				index = i;
			}
		}

		if (contours.size() != 0) {

			minEnclosingCircle((Mat) contours[index], center, radius);
			double circRatio = radius / 2;
			double accuracy =  (maxArea / perimeter) / circRatio * 100;
			cout << "radius: " << radius << "\tcirc_area_ratio: " << maxArea / (radius * radius * 3.1415926535897)  * 100 << "\tcenter: (" << center.x << ", " << center.y << ")"<< endl;
			//<< " area: " << maxArea << " perimeter: " << perimeter << " ratio: " << accuracy <<  endl;
			circle( frame, center, (int)radius, Scalar(0, 255, 0), 2, 8, 0 );
			circle( frame, center, 2, Scalar(0, 0, 255), 1, 8, 0);
			drawContours( frame, contours, index, Scalar(0, 0, 255), 2, 8, hierarchy, 0, Point() );
		}
		imshow("mask", mask);
		imshow("original", frame);
		//imshow("canny", canny_out);
		if (waitKey(30) == 27) { //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}
