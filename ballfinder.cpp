#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#define CIRCLE_DATA
#define MOUSE_DEBUG
using namespace cv;
using namespace std;

void mask_red(Mat& hsv, Mat& redMask)
{
	Mat mask1;
	Mat mask2;

	inRange(hsv, Scalar(0, 100, 100), Scalar(10, 255, 255), mask1);
	inRange(hsv, Scalar(170, 100, 100), Scalar(179, 255, 255), mask2);
	bitwise_or(mask1, mask2, redMask);

	medianBlur(redMask, redMask, 9);
}

void mask_blue(Mat& hsv, Mat& blueMask)
{
	inRange(hsv, Scalar(110, 75, 75), Scalar(130, 255, 255), blueMask);

	medianBlur(blueMask, blueMask, 9);
}

void get_contours(Mat& mask, vector<vector<Point>>& contours, vector<Vec4i>& hierarchy)
{
	findContours(mask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
}

void process_contours(vector<vector<Point>>& contours, vector<Vec4i>& hierarchy, Mat& frame) {
	for (int i = 0; i < contours.size(); i++) {

		// filter out by sheer area
		double minArea = 200;
		double area = contourArea(contours[i], false);
		if (area < minArea) continue;

		Point2f center;
		float radius;
		// filter by circle area ratio
		minEnclosingCircle((Mat) contours[i], center, radius);
		double closeness = area / (radius * radius * 3.1415926535897) * 100;
		if (closeness < 60.0) continue;
#ifdef CIRCLE_DATA
		cout << "radius: " << radius << "\t\tcloseness: " << closeness << "\tcenter: (" << center.x << ", " << center.y << ")" << endl;
#endif

		circle( frame, center, (int)radius, Scalar(0, 255, 0), 2, 8, 0 );
		circle( frame, center, 2, Scalar(0, 0, 255), 1, 8, 0);
		drawContours( frame, contours, i, Scalar(0, 0, 255), 2, 8, hierarchy, 0, Point() );
	}
}

int main( int argc, char** argv )
{
	// change this to capture(0) if you want to use a webcam
	
	VideoCapture capture;
	
	if (argc > 1) {
		capture = VideoCapture(argv[1]);
	} else {
		capture = VideoCapture(0);
	}

	if (!capture.isOpened()) {
		cerr << "Error: can't open stream!" << endl;
		return 1;
	}

	cout << "resolution: " << capture.get(CV_CAP_PROP_FRAME_WIDTH) << "x" << capture.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

	
	//namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	namedWindow("blueMask", CV_WINDOW_AUTOSIZE); 
	namedWindow("redMask", CV_WINDOW_AUTOSIZE); 
	namedWindow("original", CV_WINDOW_AUTOSIZE); 
	while (true) {
		Mat frame;

		if (!capture.read(frame)) {
			return 1;
		}

		Mat hsv;
		Mat redMask;
		Mat blueMask;

		Mat grey;
		cvtColor(frame, hsv, COLOR_BGR2HSV);
		cvtColor(frame, grey, COLOR_BGR2GRAY);

		mask_red(hsv, redMask);
		mask_blue(hsv, blueMask);


		vector<vector<Point>> redContours;
		vector<Vec4i> redHierarchy;

		vector<vector<Point>> blueContours;
		vector<Vec4i> blueHierarchy;

		get_contours(redMask, redContours, redHierarchy);
		get_contours(blueMask, blueContours, blueHierarchy);

		
		process_contours(redContours, redHierarchy, frame);
		process_contours(blueContours, blueHierarchy, frame);
		
		imshow("redMask", redMask);
		imshow("blueMask", blueMask);
		imshow("original", frame);
		if (waitKey(30) == 27) { //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}
